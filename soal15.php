<?php
/**
CONTOH:
perolehan_mendali(
array(
	array('Indonesia', 'emas')
	array('India', 'perak')
	array('Korea Selatan', )
	array('India', 'perak')
	array('India', 'emas')
	array('Indonesia', 'perak')
	array('Indonesia', 'emas')
	)
);

output:
Array(
	Array (
	[negara] => 'Indonesia'
	[emas] => 2
	[perak] => 1
	[perunggu] => 0
	),
	Array (
	[negara] => 'Korea Selatan'
	[emas] => 1
	[perak] => 0
	[perunggu] => 0
	)
)


**/
// test case
print_r (perolehan_mendali(
array(
	array('negara' => 'Indonesia', 'emas')
	array('India', 'perak')
	array('Korea Selatan', )
	array('India', 'perak')
	array('India', 'emas')
	array('Indonesia', 'perak')
	array('Indonesia', 'emas')
	)	
))
?>



