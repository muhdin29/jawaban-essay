CREATE TABLE customer (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL);
 
CREATE TABLE orders (id INT AUTO_INCREMENT PRIMARY KEY, amount VARCHAR(255) NOT NULL, customer_id INT);

alter table orders add foreign key (customer_id) references customer (id);
